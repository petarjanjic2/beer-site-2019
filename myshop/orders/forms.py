from django import forms
from .models import Order


class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['first_name', 'last_name', 'email', 'address', 'postal_code', 'city']
        labels = {
            "first_name": "Ime",
            "last_name": "Prezime",

            "address": "Adresa",
            "postal_code": "Poštanski broj",
            "city": "Grad"
                    }
