from django.shortcuts import render, get_object_or_404
from .models import Product
from cart.forms import CartAddProductForm


def product_list(request):

    products = Product.objects.filter(available=True)


    context = {

        'products': products
    }
    return render(request, 'shop/product/list.html', context)


def product_detail(request, id):
    product = get_object_or_404(Product, id=id, available=True)
    cart_product_form = CartAddProductForm()
    context = {
        'product': product,
        'cart_product_form': cart_product_form
    }
    return render(request, 'shop/product/detail.html', context)