from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.mail import send_mail

from pages.models import Content, Contact
from shop.models import Product

def index (request):

    content = Content.objects.all()
    beer = Product.objects.filter(available=True)



    return render(request, 'pages/index.html', {'contents': content,
                                                'beer_1st': beer,
                                                'products': beer,
                                                })


def contact(request):
  if request.method == 'POST':

    name = request.POST['name']
    email = request.POST['email']
    title = request.POST['title']
    message = request.POST['message']


    """#  Check if user has made inquiry already
    if request.user.is_authenticated:
      user_id = request.user.id
      has_contacted = Contact.objects.all().filter(job_id=job_id, user_id=user_id)
      if has_contacted:
        messages.error(request, 'You have already made an inquiry for this listing')
        return redirect('/listings/'+listing_id)
"""
    contact = Contact(name=name, email=email, title=title, message=message)

    contact.save()

# Send email

    send_mail(
       title,
       'Ovo je poruka za vas od '+name+':'  +message+ '.',
       email,
       ['petarjanjic2@gmail.com'],
       fail_silently=False
     )

    #messages.success(request, 'Your request has been submitted, a company will get back to you soon')
    return redirect('index')