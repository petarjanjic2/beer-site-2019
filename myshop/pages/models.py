from django.db import models

from ckeditor.fields import RichTextField

# Create your models here.
class Content(models.Model):


    title_description = RichTextField (blank=True)
    about_us = RichTextField(blank=True)
    about_beer = RichTextField(blank=True)
    beer_shop = RichTextField(blank=True)
    address = RichTextField (blank=True)
    working_time = RichTextField(blank=True)
    email = models.CharField(max_length=200, null=True)
    phone = models.CharField(max_length=200, null=True)

class Contact(models.Model):

  name = models.CharField(max_length=200)
  email = models.CharField(max_length=100)
  title = models.CharField(max_length=100)
  message = models.TextField(blank=True)

  def __str__(self):
    return self.name